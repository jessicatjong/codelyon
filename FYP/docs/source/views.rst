Views Description
===============

Question View
--------------
.. automodule:: onlinequiz.views.question_view
  :members:

Account View
--------------
.. automodule:: onlinequiz.views.account_view
  :members:

Index View
--------------
.. automodule:: onlinequiz.views.index_view
  :members:

User View
--------------
.. automodule:: onlinequiz.views.user_view
  :members: