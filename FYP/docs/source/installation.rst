Getting Started
===============

Overview
----------
These documents accompany the source code and other helper files for my Final Year Project titled "Python Online Exam
System" under the supervision of Dr. Li Fang at Nanyang Technological University, Singapore. CodeLyon is a web
platform built to facilitate learning between instructors and students. The application compiles submitted Python code
and automatically grades it, giving instant feedback for the user.

Prior Requirements
------------------

- Python 3.6.4
- MySQL 5.7.21

Installation
--------------

.. code-block:: bash

    $ git clone git@bitbucket.org:jessicatjong/codelyon.git
    $ cd FYP
    $ pip install -r requirements.txt
    $ manage.py makemigrations
    $ manage.py migrate
    $ manage.py runserver
