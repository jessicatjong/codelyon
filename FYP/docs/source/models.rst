Models Description
===============

Student
-------
.. autoclass:: onlinequiz.models.StudentUser
   :members:

Game Components
----------------
.. autoclass:: onlinequiz.models.GameCanvas
   :members:
.. autoclass:: onlinequiz.models.Hero
   :members:

Set
-------
.. autoclass:: onlinequiz.models.Set
   :members:
.. autoclass:: onlinequiz.models.PracticeSet
   :members:
.. autoclass:: onlinequiz.models.TestSet
   :members:
.. autoclass:: onlinequiz.models.GameSet
   :members:

Question
----------
.. autoclass:: onlinequiz.models.Question
   :members:
.. autoclass:: onlinequiz.models.Tag
   :members:
.. autoclass:: onlinequiz.models.MultiChoiceQuestion
   :members:
.. autoclass:: onlinequiz.models.CodeCompletionQuestion
   :members:
.. autoclass:: onlinequiz.models.HintOpened
   :members:
.. autoclass:: onlinequiz.models.Hint
   :members:
.. autoclass:: onlinequiz.models.Choice
   :members:
.. autoclass:: onlinequiz.models.TestCase
   :members:

Submission
-------------
.. autoclass:: onlinequiz.models.Submission
   :members:
.. autoclass:: onlinequiz.models.TestSubmission
   :members:
.. autoclass:: onlinequiz.models.StudentDoTest
   :members: