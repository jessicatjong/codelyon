Templates Description
===============

Templates can be found in `onlinequiz/templates` folder. The purpose of various templates are described below:

**base.html**
    Navigation bar and elements that will be displayed in every page
**index.html**
    Home page
**login.html**
    Interface for users to login
**gameBase.html**
    Base breadcrumbs and sidebar for Challenge pages

Index Pages
----------------
**gameIndex.html**
    Show all Challenge sets available
**gameIndexQn.html**
    Show all questions available in a challenge set
**modeIndex.html**
    Show sets and questions available, used in Test mode
**practiceIndex.html**
    Extension of `modeIndex.html`, used in Practice mode

Question Details
----------------
**codeEditor.html**
    Code editor and result component for Practice and Test pages
**editor.html**
    Base template for Practice and Test pages
**gameCodeEditor.html**
    Code editor and result component for Challenge page
**gameEditor.html**
    Base template for Challenge pages
**mcq.html**
    Base form for multiple choice questions
**McqEditor.html**
    MCQ question details for Practice and Test pages
**gameMcqEditor.html**
    MCQ question details for Challenge pages

Account Page
----------------
**account.html**
    Sidebar menu for all Account pages
**account_challenge.html**
    Challenge statistic page
**account_details.html**
    Account details and performance summary page
**account_edit.html**
    Edit personal particulars page
**account_submissions.html**
    View all submission list
**submission_details.html**
    View detailed information about one submission
**account_test.html**
    View all test sets that have been taken
**account_test_details.html**
    View questions in a test set

Admin Page
---------------
**test_change_form.html**
    Overrides default admin `test_change_form` page, allows instructors to view class statistics and performance
**test_summary.html**
    Overrides default admin `test_summary` page, allows instructors to view test set data