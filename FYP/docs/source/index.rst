.. CodeLyon documentation master file, created by
   sphinx-quickstart on Sat May 12 18:58:19 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



CodeLyon
====================================

.. toctree::
   :maxdepth: 2

   installation
   models
   views
   template


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
