@echo off
:: cleanup containers: docker rm `docker ps --no-trunc -aq`
:: cleanup images: docker images -q --filter "dangling=true" | xargs docker rmi
:: or docker system prune
@FOR /f "tokens=*" %%i IN ('"C:\Program Files\Docker Toolbox\docker-machine.exe" env') DO @%%i
docker build -t jessica/python-submission . >nul
docker run --rm -m 32M --stop-timeout 20 jessica/python-submission python /src/codeSubmission.py %1 %2