from django.urls import include, path, re_path
from django.views.generic.base import TemplateView
from . import views
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', auth_views.login, name='login'),
    path('check_user/', views.checkUser, name="check_user"),
    path('logout/', auth_views.logout, name='logout'),
    path('signup/', views.signup, name='signup'),
    path('account/', views.accountRedirect, name='accountRedirect'),
    path('account/acc-details/edit/', views.accountEdit, name='accountEdit'),
    path('account/<slug:current_menu>', views.account, name='account'),
    path('account/submissions/<int:id>', views.submissionDetails, name='submissionDetails'),
    path('account/test/<int:id>', views.testDetails, name='testDetails'),
    path('unlock_hint/', views.unlockHint, name='unlockHint'),
    path('test-cases/<int:id>', views.getTestCase, name='getTestCase'),
    path('ajax/load-questions', views.loadQuestion, name='loadQuestion'),
    # submit code
    re_path(r'^(?P<mode>[Practice|Challenge|Test]+)/(?P<set_id>\d+)/(?P<question_id>\d+)/submit_code/$', views.submitCodeAjax, name='submitCode'),
    # submit mcq
    re_path(r'^(?P<mode>[Practice|Challenge|Test]+)/(?P<set_id>\d+)/(?P<question_id>\d+)/submit_mcq/$', views.submitMcqAjax, name='submitMcq'),
    # display modes (practice/game/test)
    re_path(r'^(?P<mode>[Practice|Challenge|Test]+)/$', views.modeIndex, name='modeIndex'),
    # display modes' questions (practice/game/test)
    re_path(r'^(?P<mode>[Practice|Challenge|Test]+)/(?P<set_id>\d+)/$', views.modeIndexQn, name='modeIndexQn'),
    # display modes' questions (practice/game/test)
    re_path(r'^Test/(?P<set_id>\d+)/start/$', views.startTest, name='startTest'),
    # display questions
    re_path(r'^(?P<mode>[Practice|Challenge|Test]+)/(?P<set_id>\d+)/(?P<question_id>\d+)/$', views.detail, name='questionDetail'),
    #ex: /onlinequiz/5
    re_path(r'^(?P<question_id>\d+)/$', views.detail, name='detail'),
    re_path(r'^editor/$', TemplateView.as_view(
        template_name="editor.html"
    ), name='editor'),
]# + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)