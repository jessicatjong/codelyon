from django import forms
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import BaseInlineFormSet

class CodeForm(forms.Form):
    # matric = forms.CharField(label='Matric No', max_length=100)
    submission = forms.CharField(label='Submission', widget=forms.Textarea)
    points = forms.IntegerField(widget=forms.HiddenInput)
    submission_type = forms.IntegerField(widget=forms.HiddenInput)

class MCQForm(forms.Form):
    def __init__(self, *args, **kwargs):
        choice_list = kwargs.pop('choice_list', None)
        super(MCQForm, self).__init__(*args, **kwargs)
        self.fields['choice'] = forms.ModelChoiceField(queryset=choice_list, widget=forms.RadioSelect, empty_label=None)

    # choice = forms.ModelChoiceField(queryset=Choice.objects.all(), widget=forms.RadioSelect)

class SignUpForm(UserCreationForm):
    matric = forms.CharField(max_length=15, required=True)
    group = forms.CharField(max_length=20, required=False)
    char_gender = forms.ChoiceField(choices=StudentUser.GENDER_CHOICES, widget=forms.RadioSelect, required=True)
    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', )

class UserForm(forms.ModelForm):
    password = forms.CharField(required=False, widget=forms.PasswordInput, help_text="Leave blank if you do not wish to change")
    confirm_password = forms.CharField(required=False, widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = ('password', )

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            raise forms.ValidationError(
                "Password is not the same"
            )
        if not(self.instance.has_usable_password()) and not(password):
            raise forms.ValidationError(
                "Please set a password for your account"
            )



class HiddenDeleteBaseInlineFormSet(BaseInlineFormSet):
    """
    Makes the delete field a hidden input rather than the default checkbox

    inlineformset_factory(Book, Page, formset=HiddenDeleteBaseInlineFormSet, can_delete=True)
    """

    def add_fields(self, form, index):
        super(HiddenDeleteBaseInlineFormSet, self).add_fields(form, index)
        if self.can_delete:
            form.fields[DELETION_FIELD_NAME] = forms.BooleanField(
                label=_('Delete'),
                required=False,
                widget=forms.HiddenInput
            )