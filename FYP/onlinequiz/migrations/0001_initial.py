# Generated by Django 2.0.2 on 2018-03-23 18:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import markdownx.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Choice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('choice', models.TextField(blank=True, null=True, verbose_name='Choice')),
                ('correct', models.NullBooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Fight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('enemy_hp', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='GameCanvas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=1000, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='game_canvas')),
                ('pos_x', models.IntegerField(blank=True, default=0, null=True)),
                ('pos_y', models.IntegerField(blank=True, default=0, null=True)),
            ],
            options={
                'verbose_name': 'Game Asset',
            },
        ),
        migrations.CreateModel(
            name='Hero',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=1000, null=True)),
                ('proficiency', models.FloatField(blank=True, null=True)),
                ('price', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Heroes',
            },
        ),
        migrations.CreateModel(
            name='Hint',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(blank=True, null=True)),
                ('points_cost', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='HintOpened',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hint', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.Hint')),
            ],
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('difficulty', models.CharField(blank=True, choices=[('Easy', 'Easy'), ('Medium', 'Medium'), ('Hard', 'Hard')], max_length=20, null=True)),
                ('question', models.TextField(blank=True, null=True)),
                ('explanation', markdownx.models.MarkdownxField(blank=True, null=True)),
                ('points_reward', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Set',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=1000, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='StudentDoTest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('end', models.DateTimeField(blank=True, null=True)),
                ('marks', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Student test attempt',
            },
        ),
        migrations.CreateModel(
            name='StudentUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('matric', models.CharField(blank=True, max_length=200, null=True)),
                ('full_name', models.CharField(blank=True, max_length=200, null=True)),
                ('group', models.CharField(blank=True, max_length=200, null=True)),
                ('total_points', models.IntegerField(blank=True, default=0, null=True)),
                ('char_gender', models.CharField(choices=[('F', 'Female'), ('M', 'Male')], default='M', max_length=2)),
                ('current_hero', models.ForeignKey(blank=True, default=1, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.Hero')),
            ],
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.TextField(blank=True, null=True)),
                ('score', models.IntegerField(blank=True, null=True)),
                ('submission_time', models.DateTimeField(blank=True, null=True)),
                ('test_case_id', models.CharField(blank=True, max_length=10000, null=True)),
                ('test_case_result', models.CharField(blank=True, max_length=10000, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tag', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='TestCase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number_of_args', models.IntegerField(blank=True, null=True)),
                ('input_value', models.TextField(blank=True, null=True)),
                ('output_value', models.TextField(blank=True, null=True)),
                ('type', models.CharField(blank=True, choices=[('Check', 'Check'), ('Real', 'Real')], max_length=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='CodeCompletionQuestion',
            fields=[
                ('question_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='onlinequiz.Question')),
                ('default_text', models.TextField(blank=True, null=True)),
            ],
            bases=('onlinequiz.question',),
        ),
        migrations.CreateModel(
            name='GameSet',
            fields=[
                ('set_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='onlinequiz.Set')),
                ('set_difficulty', models.FloatField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Challenge Set',
            },
            bases=('onlinequiz.set',),
        ),
        migrations.CreateModel(
            name='MultiChoiceQuestion',
            fields=[
                ('question_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='onlinequiz.Question')),
            ],
            options={
                'verbose_name': 'Multiple choice question',
            },
            bases=('onlinequiz.question',),
        ),
        migrations.CreateModel(
            name='PracticeSet',
            fields=[
                ('set_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='onlinequiz.Set')),
            ],
            bases=('onlinequiz.set',),
        ),
        migrations.CreateModel(
            name='TestSet',
            fields=[
                ('set_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='onlinequiz.Set')),
                ('time_limit', models.PositiveIntegerField(blank=True, null=True)),
                ('start_time', models.DateTimeField(blank=True, null=True)),
                ('end_time', models.DateTimeField(blank=True, null=True)),
                ('status', models.NullBooleanField()),
                ('allow_avg', models.NullBooleanField()),
                ('allow_set_score', models.NullBooleanField()),
                ('allow_set_details', models.NullBooleanField()),
                ('allow_qn_details', models.NullBooleanField()),
                ('allow_test_case', models.NullBooleanField()),
                ('allow_qn_score', models.NullBooleanField()),
            ],
            bases=('onlinequiz.set',),
        ),
        migrations.CreateModel(
            name='TestSubmission',
            fields=[
                ('submission_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='onlinequiz.Submission')),
                ('feedback', markdownx.models.MarkdownxField(blank=True, null=True)),
            ],
            bases=('onlinequiz.submission',),
        ),
        migrations.AddField(
            model_name='submission',
            name='choice',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.Choice'),
        ),
        migrations.AddField(
            model_name='submission',
            name='question',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.Question'),
        ),
        migrations.AddField(
            model_name='submission',
            name='set',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.Set'),
        ),
        migrations.AddField(
            model_name='submission',
            name='student',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.StudentUser'),
        ),
        migrations.AddField(
            model_name='studentuser',
            name='question',
            field=models.ManyToManyField(through='onlinequiz.Submission', to='onlinequiz.Question'),
        ),
        migrations.AddField(
            model_name='studentuser',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='studentdotest',
            name='student',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.StudentUser'),
        ),
        migrations.AddField(
            model_name='set',
            name='question',
            field=models.ManyToManyField(to='onlinequiz.Question'),
        ),
        migrations.AddField(
            model_name='question',
            name='tag',
            field=models.ManyToManyField(related_name='questions', to='onlinequiz.Tag'),
        ),
        migrations.AddField(
            model_name='hintopened',
            name='student',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.StudentUser'),
        ),
        migrations.AddField(
            model_name='hint',
            name='question',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.Question'),
        ),
        migrations.AddField(
            model_name='gamecanvas',
            name='rank_req',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.Hero'),
        ),
        migrations.AddField(
            model_name='fight',
            name='student',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.StudentUser'),
        ),
        migrations.AddField(
            model_name='testsubmission',
            name='student_do_test',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.StudentDoTest'),
        ),
        migrations.AddField(
            model_name='testcase',
            name='question',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.CodeCompletionQuestion'),
        ),
        migrations.AddField(
            model_name='studentuser',
            name='test',
            field=models.ManyToManyField(through='onlinequiz.StudentDoTest', to='onlinequiz.TestSet'),
        ),
        migrations.AddField(
            model_name='studentdotest',
            name='set',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.TestSet'),
        ),
        migrations.AddField(
            model_name='fight',
            name='set',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.GameSet'),
        ),
        migrations.AddField(
            model_name='choice',
            name='question',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='onlinequiz.MultiChoiceQuestion'),
        ),
        migrations.CreateModel(
            name='TestSetSummary',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
            },
            bases=('onlinequiz.testset',),
        ),
    ]
