import docker
from subprocess import check_output

command = '@FOR /f \"tokens=*\" %i IN (\'\"C:\\Program Files\\Docker Toolbox\\docker-machine.exe\" env\') DO @%i'
check_output(command, shell=True)
client = docker.from_env()
print(client.containers.run("ubuntu:latest", "echo hello world"))