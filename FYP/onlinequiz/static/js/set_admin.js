$( document ).ready( function() {
    $('#id_diff, #id_tags').select2({
       width: '15rem',
    });
    $('#filter_button').prev().remove();
    $('#filter_button').parent().css('margin-top', '5px')
    $("#id_question").css('width', '50%');
    $('#id_question').select2({
    });

    // Must remove so ajax doesn't overlap, store temp value meanwhile
    var temp_val = ($('#id_question').val());
    $("#id_question").on("loadQn", function(){
        var url = "/onlinequiz/ajax/load-questions";
        var tag_id = $("#id_tags").val();
        var diff_id = $("#id_diff").val();
        $.ajax({
            url: url,
            data: {
                'tag_id': tag_id,
                'diff_id': diff_id
            },
            success: function (data) {
                // Sorry for using global vars.. Quick hack
                window.loadResults = data.results;
                $('#id_question').trigger('change');
               $("#id_question").select2({
                  data: data.results,
                  escapeMarkup: function(markup) {
                    return markup;
                  },
                  templateResult: function(data) {
                      // For optgroups
                      if (!data.id) {
                        return data.text;
                      }

                      var qn = '<div class="' +
                          data.diff + " " + data.tag.replace(/(?<=[a-zA-Z])\s(?=[a-zA-Z])/g, '-').replace(/,/g, '') +
                          '"><a style="color:darkcyan;">' + data.text + '</a>';
                      if (data.diff) {
                          qn +=  ' (' +  data.diff + ')</div>'
                      } else {
                          qn += '</div>'
                      }
                      if (data.tag) {
                          qn += '<div><small>' + data.tag + '</small></div>';
                      }
                      return qn
                  },
                  templateSelection: function(data) {
                    return data.text;
                  }
                });
            }
        });
    });
    $("#id_question").trigger( "loadQn" );

    // restore old value
    $('#id_question').val(temp_val);
    $('#id_question').trigger('change');

    // Hide elements that are not filtered
    $("#id_question").on("select2:open", function() {
        function mapToStr(toSearch) {
            filter_class = jQuery.map(toSearch, function (n) {
                return ( "." + n );
            });
            filter_class = filter_class.join(", ");
            filter_class = ':not(' + filter_class + ')';
            return filter_class
        }
        var to_search_tag= [];
        var to_search_diff= [];
        var tag_selection = $('#id_tags').val();
        var diff_selection = $('#id_diff').val();
        if (tag_selection.length){
            for(var i=0; i<tag_selection.length; i++) {
                to_search_tag.push(tag_selection[i]);
            }
            to_search_tag = mapToStr(to_search_tag)
        }
        if (diff_selection.length){
            for(var i=0; i<diff_selection.length; i++) {
                to_search_diff.push(diff_selection[i]);
            }
            to_search_diff = mapToStr(to_search_diff)
        }

        if (tag_selection.length || diff_selection.length) {
            var checkExist = setInterval(function () {
                if ($('#select2-id_question-results').length) {
                    clearInterval(checkExist);
                    if (tag_selection.length) {$("#select2-id_question-results div:first-child").filter(to_search_tag).parent().remove();}
                    if (diff_selection.length) {$("#select2-id_question-results div:first-child").filter(to_search_diff).parent().remove();}
                    if ($('#select2-id_question-results li').length == 0) {
                        $("#select2-id_question-results").append(
                            '<li class="select2-results__option">No question matching your filter</li>'
                        )
                    }

                }
            }, 10);
        }
    });

    // Selected List
    var selectText = '<div class="form-row field-selectedqn" style="max-height: 300px; overflow: auto; width: 40%">\
                    <label>Selected Questions:</label>\
                    <table id="selected-qn-ul"></table>\
                    </div>';
    $(selectText).insertAfter(".field-question");
    $("#id_question").on("change", function(){
        $("#selected-qn-ul").empty();
        var data = window.loadResults;
        var selectedId = $('#id_question').val();
        var wanted = data.filter(i => $.inArray( i.id.toString(), selectedId ) != -1 );
        if(wanted.length > 0) {
            var removeAll = '<tr><td id="removeAll" style="border-bottom-width:0; padding:0 0 4px 0 "><a class="remove-link">Remove all</a></td></tr>'
            $("#selected-qn-ul").append(
                removeAll
            );
        }
        for (var i=0; i<wanted.length; i++) {
            var removeDiv = '<div class="removeDiv" internal-id="'+ wanted[i].id +
                '"><i class="fa fa-times" style="color:red;"></i> <a class="remove-link">Remove</a></div>';
            if (wanted[i].tag) {
                var removeSelect =  '<tr>\
                <td style="border-bottom-width:0; padding:4px 0 0 0"><a style="color: darkcyan">' + wanted[i].text + '</a> ('+ wanted[i].diff +')' + '</td>' +
                    '<td style="vertical-align: middle" rowspan="2">' + removeDiv +'</td>\
                </tr><tr><td style="padding:0 0 4px 0 "><small>'+wanted[i].tag+'</small></td></tr>';
            }
            else {
                var removeSelect = '<tr>\
                <td style="padding-left:0"><a style="color: darkcyan">' + wanted[i].text + '</a> ('+ wanted[i].diff +')' + '</td>' +
                    '<td>' + removeDiv + '</td></tr>'
            }
            $("#selected-qn-ul").append(
                removeSelect
            );
        }
    });

    // To remove selection
    $('body').on('click', '#removeAll', function() {
        $("#id_question option:selected").prop("selected", false);
        $('#id_question').trigger('change');
    });

    $('body').on('click', '.removeDiv', function() {
        $("#id_question option[value='"+ $(this).attr("internal-id") +"']").prop("selected", false);
        $('#id_question').trigger('change');
    });

});