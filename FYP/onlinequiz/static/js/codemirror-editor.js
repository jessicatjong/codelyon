$( document ).ready(function() {
    var textArea = document.getElementById('id_default_text');
    var editor = CodeMirror.fromTextArea(textArea, {
        mode: {
            name: "python",
            version: 3,
            singleLineStringErrors: false
        },
        lineNumbers: true,
        matchBrackets: true,
        onChange: function () {
            editor.save()
        },
    });
});