from django.contrib import admin
from markdownx.admin import MarkdownxModelAdmin
from markdownx.widgets import AdminMarkdownxWidget
from .models import *
from django import forms
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from import_export import resources, widgets, fields
from import_export.admin import ImportExportModelAdmin, ExportMixin, ExportActionModelAdmin
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
import re

admin.site.site_header = 'CodeLyon'

class UserResource(resources.ModelResource):
    class Meta:
        model = User
        skip_unchanged = True
        report_skipped = True
        fields = ('id', 'username')
    def import_obj(self, obj, data, dry_run):
        name = data.get('Name')
        username = generate_username(name)
        last_id = User.objects.order_by('-id')[0].id
        for id, field in enumerate(self.get_fields(), last_id+1):
            if isinstance(field.widget, widgets.ManyToManyWidget):
                continue
            if field.column_name =='id':
                data.update({'id': id})
            if field.column_name == 'username':
                data.update({'username': username})
            print(obj)
            self.import_field(field, obj, data)

    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        dataset.insert_col(0, col=["", ] * dataset.height, header="id")

    def get_instance(self, instance_loader, row):
        return False

    def after_import_row(self, row, row_result, **kwargs):
        try:
            group = row['Group']
        except KeyError:
            group = None
        name = row['Name']
        user = User.objects.get(id=row['id'])
        s = StudentUser(user=user, current_hero=Hero.objects.all().order_by('price')[0],
                            group=group, full_name=name)
        s.save()
        return
# class UserAdmin(ImportExportModelAdmin):
#     resource_class = UserResource

def generate_username(name):
    name = re.sub("[^a-z]", "", name.lower())
    if User.objects.filter(username=name).exists():
        prefix = 1
        while User.objects.filter(username=name+str(prefix)).exists():
            prefix += 1
        name += str(prefix)
    return name

class StudentInline(admin.StackedInline):
    model = StudentUser
    can_delete = False
    verbose_name_plural = 'studentUser'

# Define a new User admin
class UserAdmin(ImportExportModelAdmin):
    resource_class = UserResource
    inlines = (StudentInline, )
    list_display = ('id', 'username')

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 2
    def get_extra(self, request, obj=None, **kwargs):
        """Dynamically sets the number of extra forms. 0 if the related object
        already exists or the extra configuration otherwise."""
        if obj:
            return 0
        return self.extra

class HintInline(admin.TabularInline):
    model = Hint
    extra = 1
    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 0
        return self.extra



class TestCaseForm(forms.ModelForm):
    output_value = forms.CharField(widget=forms.Textarea(attrs={'rows':3, 'cols': 40},), required=False)
    class Meta:
        model = TestCase
        fields = '__all__'

class TestCaseInline(admin.TabularInline):
    model = TestCase
    form = TestCaseForm
    extra = 1
    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 0
        return self.extra

class MCQModelForm(forms.ModelForm):
    question = forms.CharField()
    class Meta:
        model = Question
        fields = '__all__'

class MCQAdmin(admin.ModelAdmin):
    formfield_overrides = {
        MarkdownxField: {'widget': AdminMarkdownxWidget}
    }
    list_display = ('id', 'question', 'short_description', 'difficulty')
    list_filter = ('difficulty',)
    search_fields = ('question', 'id')
    form = MCQModelForm
    inlines = [
        ChoiceInline,
        HintInline
    ]

class CodeCompletionModelForm(MCQModelForm):
    pass

class QuestionAdmin(admin.ModelAdmin):
    formfield_overrides = {
        MarkdownxField: {'widget': AdminMarkdownxWidget}
    }
    list_display = ('id', 'question', 'short_description', 'difficulty')
    list_filter = ('difficulty',)
    search_fields = ('question', 'id')

class CodeCompletionAdmin(QuestionAdmin):
    class Media:
        css = {
            'all': (
                "onlinequiz/codemirror-5.33.0/lib/codemirror.css",
            )
        }
        js = (
            "//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js",
            "onlinequiz/codemirror-5.33.0/lib/codemirror.js",
            "onlinequiz/codemirror-5.33.0/mode/python/python.js",
            "js/codemirror-editor.js",
        )
    list_display = ('id', 'question', 'short_description', 'difficulty')
    list_filter = ('difficulty',)
    form = CodeCompletionModelForm
    inlines = [
        HintInline,
        TestCaseInline
    ]

class TestCaseAdmin(admin.ModelAdmin):
    form = TestCaseForm

class SetForm(forms.ModelForm):
    tags = forms.MultipleChoiceField(choices=[(str.replace(tag.tag, " ", "-"), tag.tag) for tag in Tag.objects.all()], help_text="Filter questions by tag",
                                     required=False)
    diff = forms.MultipleChoiceField(choices=DIFFICULTY_CHOICES, label='Difficulty', help_text="Filter questions by difficulty",
                                     required=False)
    question = forms.ModelMultipleChoiceField(queryset=Question.objects.all(), required=False, label='Add New Questions',
                                              help_text="Add questions by typing in the box above",)

    class Meta:
        model = Set
        fields = '__all__'
    # def __init__(self, *args, **kwargs):
    #     super(SetForm, self).__init__(*args, **kwargs)
    #     wtf = Question.objects.all()
    #     w = self.fields['question'].widget
    #     choices = []
    #     for choice in wtf:
    #         choices.append((choice.id, choice.question))
    #     w.choices = choices
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['question'].queryset = Question.objects.all()

class SetAdmin(admin.ModelAdmin):
    # filter_horizontal = ('question', )
    search_fields = ('name', 'id')
    list_display = ('id', 'name')
    form = SetForm
    class Media:
        css = {
            'all': (
                "//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css",
                "//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
                "css/admin.css"
            )
        }
        js = (
            "//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js",
            "js/jquery.waituntilexists.js",
            "//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js",
            "js/set_admin.js",
            "//cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.0.2/jquery.sumoselect.min.js",
            #"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        )

class GameSetAdmin(SetAdmin):
    fields = ('name', ('tags', 'diff'), 'question', 'set_difficulty')

    list_display = SetAdmin.list_display + ('set_difficulty',)

class PracticeSetAdmin(SetAdmin):
    fields = ('name', ('tags', 'diff'), 'question')

class TestSetAdmin(SetAdmin):
    fields = ('name', ('tags', 'diff'), 'question', 'time_limit', 'start_time', 'end_time',
              ('allow_avg', 'allow_set_score', 'allow_set_details', 'allow_qn_details', 'allow_test_case', 'allow_qn_score'))
    list_display = SetAdmin.list_display + ('status', 'start_time', 'end_time', 'time_limit')
    list_filter = ('start_time', 'end_time', 'status')

class SubmissionInline(admin.TabularInline):
    model = TestSubmission
    exclude = ['student', 'set', 'submission_time']
    extra = 1
    formfield_overrides = {
        MarkdownxField: {'widget': AdminMarkdownxWidget(attrs={'rows':3, 'cols': 5},)},
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 4, 'cols': 40})},
        models.CharField: {'widget': forms.TextInput(attrs={'size': '15'})},
    }
    def get_extra(self, request, obj=None, **kwargs):
        if obj:
            return 0
        return self.extra

class StudentDoTestResource(resources.ModelResource):
    class Meta:
        model = StudentDoTest
        fields = ('id', 'student__full_name', 'student__matric', 'student__group', 'set__name', 'marks')
        export_order = ('id', 'student__full_name', 'student__matric', 'student__group', 'set__name', 'marks')

class StudentDoTestAdmin(ExportMixin, admin.ModelAdmin):
    resource_class = StudentDoTestResource
    search_fields = ('id', 'student__matric', 'set__name')
    list_display = ('id', 'student', 'set', 'marks')
    list_filter = ('set',)
    inlines = [
        SubmissionInline
    ]

@admin.register(TestSetSummary)
class TestSetSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'admin/test_summary.html'
    change_form_template = 'admin/test_change_form.html'
    search_fields = ('id', 'name')

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        response = super().changeform_view(
            request,
            extra_context=extra_context,
            object_id=object_id,
            form_url=form_url,
        )
        try:
            qs = response.context_data['original']
        except (AttributeError, KeyError):
            return response

        # First table
        metricsSet = {
            'avg': Avg('marks'),
            'max': Max('marks'),
            'min': Min('marks'),
        }
        response.context_data['setsummary'] = qs.studentdotest_set.all().aggregate(**metricsSet)

        # Second table
        metrics = {
            'total': Count('id'),
            'avg': Avg('score'),
            'max': Max('score'),
            'min': Min('score'),
        }
        # Count no. of attempts / question, class avg., max, min
        response.context_data['summary'] = list(
            qs.submission_set.all().values('question', 'question__question').annotate(**metrics)
        )

        # Chart
        response.context_data['chart'] = list(qs.submission_set.all().values('question__question'))

        return response

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )
        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response
        metrics = {
            'total': Count('studentdotest'),
            'avg': Avg('studentdotest__marks'),
            'max': Max('studentdotest__marks'),
            'min': Min('studentdotest__marks'),
        }
        response.context_data['summary'] = list(
            qs.values('id', 'name').annotate(**metrics)
        )
        return response

    class Media:
        css = {
            'all': (
                "//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
            )
        }
        js = (
            "//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js",
            "//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js",
        )

class SubmissionForm(forms.ModelForm):
    code = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Question
        fields = '__all__'

class SubmissionAdmin(admin.ModelAdmin):
    date_hierarchy = 'submission_time'
    search_fields = ('id', 'student__matric', 'question__question', 'set__name')
    list_display = ('id', 'student', 'question', 'score', 'set')
    list_filter = ('set',)
    form = SubmissionForm

class GameCanvasAdmin(admin.ModelAdmin):
    search_fields = ('id', 'name', 'image')
    list_display = ('id', 'name', 'image')

# Register your models here.
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(TestSubmission, SubmissionAdmin)
admin.site.register(MultiChoiceQuestion, MCQAdmin)
admin.site.register(CodeCompletionQuestion, CodeCompletionAdmin)
admin.site.register(Choice)
admin.site.register(StudentUser)
admin.site.register(StudentDoTest, StudentDoTestAdmin)
admin.site.register(Hint)
admin.site.register(Set, SetAdmin)
admin.site.register(PracticeSet, PracticeSetAdmin)
admin.site.register(TestSet, TestSetAdmin)
admin.site.register(GameSet, GameSetAdmin)
admin.site.register(Hero)
admin.site.register(Tag)
admin.site.register(GameCanvas, GameCanvasAdmin)
admin.site.register(HintOpened)
admin.site.register(Question, QuestionAdmin)
admin.site.register(TestCase, TestCaseAdmin)