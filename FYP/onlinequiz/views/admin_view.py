from django.http import JsonResponse
from onlinequiz.forms import *
from onlinequiz.models import *

def loadQuestion(request):
    tag_id = request.GET.getlist('tag_id[]')
    diff_id = request.GET.getlist('diff_id[]')
    data = {
        "results": [
        ]
    }
    questions = Question.objects.all()
    if diff_id:
        questions = questions.filter(difficulty__in=diff_id)
    if tag_id:
        questions = questions.filter(tag__id__in=tag_id)
        tags = Tag.objects.filter(id__in=tag_id)
    else:
        tags = Tag.objects.all()
    questions = questions.distinct()
    for question in questions:
        data["results"].append(
            {
                "id": question.id,
                "text": question.question,
                "tag": question.tag_comma,
                "diff": question.difficulty,
            })
    return JsonResponse(data)