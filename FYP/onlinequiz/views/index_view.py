from django.shortcuts import get_object_or_404, render, redirect
from django.core import serializers
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.template import loader
from django.db.models.aggregates import Max
from django.db.models import IntegerField
from onlinequiz.forms import *
from django.forms.models import inlineformset_factory
import html
from onlinequiz.models import *
import datetime
import os
import subprocess
from subprocess import PIPE, Popen
import filecmp
import json
import math
from markdownx.utils import markdownify

modeDict = {
    'Practice': PracticeSet.objects.all(),
    'Challenge': GameSet.objects.all().order_by('set_difficulty'),
    'Test': TestSet.objects.all(),
}

def getTestCase(request, id):
    """
    Displays the test case input value of a particular question. Used during 'Submit' flow where the student can click
    on a question's test case see its details

    :param request: submitted request
    :param id: test case id
    :type request: HttpRequest
    :type id: int
    :return: input value of a test case
    :rtype: HttpResponse
    """
    testCase = TestCase.objects.get(id=id).input_value
    return HttpResponse(testCase)

def index(request):
    return render(request, 'onlinequiz/index.html')

def modeIndex(request, mode):
    """
    Displays the available sets in a mode

    :param request: submitted request
    :param mode: current mode
    :type request: HttpRequest
    :type mode: str
    :return: renders page with set list
    """
    if mode=='Practice' or mode=='Test':
        set_id = modeDict[mode][0]
        return redirect('modeIndexQn', mode=mode, set_id=set_id.id)

    if mode=='Challenge':
        context = {
            'mode': mode,
            'available_sets': modeDict[mode],
            'leaderboard': StudentUser.objects.filter().order_by('-total_points')[:5]
        }
        context['remaining_hp'] = []
        for i in context['available_sets']:
            context['remaining_hp'].append(Submission.objects.filter(student=request.user.studentuser, set=i, score__gte=75).values('question').distinct().count())
        context['remaining_hp'] = reshapeQuerySet(context['remaining_hp'], 6)
        context['available_sets'] = reshapeQuerySet(context['available_sets'], 6)
        context['zipped'] = zip(context['available_sets'], context['remaining_hp'])
        context['page'] = range(len(context['available_sets']))
    return render(request, 'onlinequiz/gameIndex.html', context)

def modeIndexQn(request, mode, set_id):
    """
    Displays the available questions in a set

    :param request: submitted request
    :param mode: current mode
    :param set_id: set id
    :type request: HttpRequest
    :type mode: str
    :type set_id: int
    :return: renders the page with the question list
    """
    sets = modeDict[mode]
    set = Set.objects.get(id=set_id)
    questions = set.question.all()
    difficulty = questions.values_list('difficulty', flat=True).distinct()
    # Sort difficulty
    d = {"Easy": 1,
         "Medium": 2,
         "Hard": 3}
    difficulty = sorted(difficulty, key=lambda x: d[x])

    links = {
        'Practice' : 'onlinequiz/practiceIndex.html',
        'Challenge' : 'onlinequiz/gameIndexQn.html',
        'Test' : 'onlinequiz/testIndex.html'
    }
    context = {
        'mode': mode,
        'set_id':int(set_id),
        'set': set,
        'available_sets': sets,
        'questions': questions,
        'difficulties': difficulty,
    }

    # Get all student submissions for this set
    qn_sub = []
    if hasattr(set, 'testset'):
        completed = Submission.objects.filter(student=request.user.studentuser, set=set)
    else:
        completed = Submission.objects.filter(student=request.user.studentuser, set=set, score__gte=75)
    for question in questions:
        sub = completed.filter(question=question).order_by('-score')
        if (sub):
            qn_sub.append((question, sub[0].score))
        else:
            qn_sub.append((question, False))
    context['qn_sub'] = qn_sub

    if hasattr(set, 'testset'):
        context.update(checkTestStatus(request.user.studentuser, set.testset))
    elif hasattr(set, 'gameset'):
        context['remaining_hp'] = Submission.objects.filter(student=request.user.studentuser, set=set, score__gte=75).values('question').distinct().count()
    return render(request, links[mode], context)

def reshapeQuerySet(query_set, w):
    """
    Reshapes Django's query set into width w, used for rendering boxes in Challenge mode Set Index. e.g. query_set=
    12 and w=6 is reshaped into size[2][6]

    :param query_set: the queryset to be reshaped
    :param w: width of list
    :type query_set: QuerySet
    :type w: int
    :return: reshaped query_set
    :rtype: list
    """
    result = []
    n = len(query_set)
    leftover = n % w
    for start, end in zip(range(0, n, w), range(w, n, w)):
        result.append(query_set[start:end])
    if leftover:
        result.append(query_set[ math.floor(n/w)*w : ])
    return result

def checkTestStatus(student, set):
    """
    Checks if student is still allowed to continue doing a test set by checking the start time, end time, and time left

    :param student: the student
    :param set: the current test set
    :type student: StudentUser
    :type set: Set
    :return: returns status of student and test
    :rtype: dict
    """
    context = {}
    if set.start_time < datetime.datetime.now() and set.end_time > datetime.datetime.now():
        context['status'] = 'havent_started'
        context['time_left'] = checkEndTime(student, set)
        # False if time_left doesn't return anything
        if context['time_left']:
            if checkEndTime(student, set, json_time=False) < datetime.datetime.now():
                context['status'] = 'time_up'
            else:
                context['status'] = 'doing_test'
        context['test_time'] = set.time_limit
        context['end_time'] = set.end_time.strftime("%d/%m/%Y %H:%M:%S")
    else:
        context['status'] = 'not_available'
    return context

def startTest(request, set_id):
    """
    Starts a test for the student, countdown is started

    :param request: submitted request
    :param set_id: the current set id
    :type request: HttpRequest
    :type set_id: int
    :return: displays question list for the test set
    :rtype: redirect
    """
    student = request.user.studentuser
    set = Set.objects.get(id=set_id)
    studentStartTest(student, set.testset)
    return redirect('modeIndexQn', 'Test', set_id)

def studentStartTest(student, currentSet):
    """
    Calculates the time limit for the student to do the test

    :param student: the student
    :param currentSet: the current test set
    :type student: StudentUser
    :type currentSet: Set
    :return: JSON string of the end time in ISO formatting
    :rtype: str
    """
    timeLimit = currentSet.time_limit
    endTime = datetime.datetime.now() + datetime.timedelta(minutes=timeLimit)
    startTest = StudentDoTest(student=student, end=endTime, set=currentSet.testset)
    startTest.save()
    return json.dumps(endTime.isoformat())

def checkEndTime (student, currentSet, json_time=True):
    """
    Checks the end time for a student to do a test set

    :param student: the student
    :param currentSet: the current test set
    :param json_time: specifies if the time should be returned in json or time format
    :type student: StudentUser
    :type currentSet: Set
    :return: end time
    """
    studentDoTest = StudentDoTest.objects.filter(student=student).filter(set=currentSet.testset)
    if not studentDoTest:
        return False
    else:
        studentDoTest = studentDoTest[0]
        endTime = studentDoTest.end
        if json_time:
            return json.dumps(endTime.isoformat())
        else:
            return endTime

def detail(request, mode, set_id, question_id):
    """
    Displays the question details, i.e. title, explanation, format (MCQ/Code Completion), and all details

    :param request: submitted request
    :param mode: current mode
    :param set_id: set id
    :param question_id: question id
    :type request: HttpRequest
    :type mode: str
    :type set_id: int
    :type question_id: int
    :return: renders question detail page
    """
    student = request.user.studentuser
    question = Question.objects.get(pk=question_id)
    if question.explanation:
        question.explanation = markdownify(question.explanation)
    set = Set.objects.get(id=set_id)
    context = {
        'set': set,
        'question': question,
        'mode': mode,
    }
    # Question exist in set?
    chk = set.question.filter(question=question)
    if not chk:
        return HttpResponseRedirect('/onlinequiz/' + mode)
    if hasattr(set, 'gameset'):
        page = 'gameCodeEditor.html'
        if hasattr(question, 'multichoicequestion'):
            choices = question.multichoicequestion.choice_set.all()
            form = MCQForm(choice_list=choices)
            page = 'gameMcqEditor.html'
        else:
            form = CodeForm()
            context['defaultText'] = html.unescape(question.codecompletionquestion.default_text)

        # Get canvas assets
        ownedHero = Hero.objects.filter(price__lte=student.total_points)
        context['canvas_asset'] = GameCanvas.objects.filter(rank_req__in=ownedHero).order_by('rank_req__id')
        context['canvas_asset_json'] = serializers.serialize('json', context['canvas_asset'], fields=('image', 'pos_x', 'pos_y'))

        context['form'] = form
        context['hints'] = question.hint_set.all()
        context['hints_opened'] = [HintOpened.objects.filter(student = student, hint=hint).exists() for hint in context['hints']]
        context['zipped'] = zip(context['hints'], context['hints_opened'])
        context['hint_cost'] = [hint.points_cost for hint in context['hints']]
        return render(request, page, context)

    if hasattr(question, 'multichoicequestion'):
        choices = question.multichoicequestion.choice_set.all()
        form = MCQForm(choice_list=choices)
        page = 'mcqEditor.html'

    else:
        form = CodeForm()
        page = 'codeEditor.html'
        context['defaultText'] = html.unescape(question.codecompletionquestion.default_text)
    context['form'] = form
    context['hints'] = question.hint_set.all()
    context['hint_cost'] = [hint.points_cost for hint in context['hints']]
    if hasattr(set, 'testset'):
        # Student already submit question before?
        s = TestSubmission.objects.filter(student=student, set=set.testset, question=question)
        if s:
            return HttpResponseRedirect('/onlinequiz/Test')
        context.update(checkTestStatus(student, set.testset))
        if context.get("status") != "doing_test":
            return HttpResponseRedirect('/onlinequiz/Test')
    return render(request, page, context)