from django.shortcuts import get_object_or_404, render, redirect
from django.http import JsonResponse
from onlinequiz.forms import *
from onlinequiz.models import *
from django.contrib.auth import login, authenticate


def checkUser(request):
    """
    Checks if it is the first time for students logging in. If yes, allow students to set their password

    :param request: submitted request
    :type request: HttpRequest
    :return: returns user status
    :rtype: JsonResponse
    """
    if request.method == 'POST':
        username = request.POST.get('username')
        user = User.objects.filter(username=username)
        context = {
            'success': False
        }
        if user.exists():
            if not(user[0].has_usable_password()):
                context['success'] = True
                login(request, user[0])
        return JsonResponse(status=200, data=context)

# Not used, user is created through import/export
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            s = StudentUser(user=user, matric=form.cleaned_data.get('matric'), group=form.cleaned_data.get('group'),
                            total_points=0, char_gender=form.cleaned_data.get('char_gender'),
                            current_hero=Hero.objects.all().order_by('price')[0])
            s.save()
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})