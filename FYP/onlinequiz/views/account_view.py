from django.shortcuts import get_object_or_404, render, redirect
from django.core import serializers
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.template import loader
from django.db.models.aggregates import Max
from django.db.models import IntegerField
from onlinequiz.forms import *
from django.forms.models import inlineformset_factory
import html
from onlinequiz.models import *
from markdownx.utils import markdownify

menu_list = {
    'acc-details': {
        'name': 'Account Details',
        'html': 'onlinequiz/account_details.html'
    },
    'submissions': {
        'name': 'Submissions',
        'html': 'onlinequiz/account_submissions.html'
    },
    'challenge': {
        'name': 'Challenge Statistics',
        'html': 'onlinequiz/account_challenge.html'
    },
    'test': {
        'name': 'Test Results',
        'html': 'onlinequiz/account_test.html'
    },
}

def accountEdit(request):
    """
    Allows student to edit their particulars

    :param request: submitted request
    :type request: HttpRequest
    :return: Renders account_edit page
    """
    context = {
        'menu_list': {k: v['name'] for k, v in menu_list.items()},
        'current_menu': 'acc-details'
    }
    user = request.user
    user_form = UserForm(instance=user)
    ProfileInlineFormset = inlineformset_factory(User, StudentUser, fields=('matric', 'group', 'char_gender'), can_delete=False)
    formset = ProfileInlineFormset(instance=user)

    if request.method == "POST":
        user_form = UserForm(request.POST, instance=user)
        formset = ProfileInlineFormset(request.POST, instance=user)

        if user_form.is_valid():
            password = user_form.cleaned_data['password']
            if (password):
                created_user = user_form.save(commit=False)
                created_user.set_password(password)
                created_user.save()
            formset.save()
            return HttpResponseRedirect('/onlinequiz/account/acc-details')

    context["form"] = user_form
    context["formset"] = formset
    return render(request, "onlinequiz/account_edit.html", context)

def accountRedirect(request):
    return redirect('account', current_menu='acc-details')

def account(request, current_menu):
    """
    Handles display for submission & test list, challenge statistics, and account details

    :param request: submitted request
    :param current_menu: the current menu the user is located at
    :type request: HttpRequest
    :type current_menu: str
    :return: Renders the corresponding page
    """
    context = {
        'menu_list': {k: v['name'] for k, v in menu_list.items()},
        'current_menu': current_menu,
    }
    student = request.user.studentuser
    if current_menu == 'submissions':
        context['submissions'] = Submission.objects.filter(student=student).order_by('-submission_time')
    elif current_menu == 'challenge':
        # Get the next hero
        next_hero = Hero.objects.all().filter(price__gt=student.current_hero.price).order_by('price')
        if next_hero:
            context['next_hero'] = next_hero[0]
            context['price_diff'] = context['next_hero'].price - student.current_hero.price
            context['progress'] = student.total_points - student.current_hero.price

        # Get canvas assets
        ownedHero = Hero.objects.filter(price__lte=student.total_points)
        context['canvas_asset'] = GameCanvas.objects.filter(rank_req__in=ownedHero).order_by('rank_req__id')
        context['canvas_asset_json'] = serializers.serialize('json', context['canvas_asset'], fields=('image', 'pos_x', 'pos_y'))
    elif current_menu == 'test':
        context['sets'] = StudentDoTest.objects.filter(student=student)
    elif current_menu == 'acc-details':
        submission = Submission.objects.filter(student=student).exclude(question__tag__isnull=True)
        context['summary'] = list(submission.values('question__tag__tag').annotate(count=Count('id'), avg=Avg('score', output_field=IntegerField())))
    return render(request, menu_list[current_menu]['html'], context)

def submissionDetails(request, id):
    """
    Displays the student's past submission details for one question

    :param request: submitted request
    :param id: submission id
    :type request: HttpRequest
    :type id: int
    :return: Renders the submission_details page
    """
    submission = Submission.objects.get(id=id)
    if submission.question.explanation:
        submission.question.explanation = markdownify(submission.question.explanation)
    context = {
        'menu_list': {k: v['name'] for k, v in menu_list.items()},
        'current_menu': 'test' if hasattr(submission.set, 'testset') else 'submissions',
        'submission': submission,
    }
    if submission.test_case_id:
        test_case = submission.test_case_id.split(' ')
        context['test_cases'] = test_case

    return render(request, 'onlinequiz/submission_details.html', context)

def testDetails(request, id):
    """
    Displays the breakdown of a question list for a particular test set

    :param request: submitted request
    :param id: StudentDoTest id
    :type request: HttpRequest
    :type id: int
    :return: Renders the account_test_details page
    """
    sdt = StudentDoTest.objects.get(id=id)
    submissions = sdt.testsubmission_set.all()
    context = {
        'menu_list': {k: v['name'] for k, v in menu_list.items()},
        'current_menu': 'test',
        'submissions': submissions,
        'set': sdt.set,
        'sdt': sdt,
    }
    return render(request, 'onlinequiz/account_test_details.html', context)