from django.core import serializers
from django.http import JsonResponse
from django.db.models.aggregates import Max
from onlinequiz.forms import *
from onlinequiz.models import *
import datetime
import os
import subprocess
from subprocess import PIPE
import filecmp

def changePoints(student, op, amount):
    """
    Changes a student's points by a certain amount

    :param student: the student
    :param op: operator to use, either 'add' or 'sub'
    :param amount: amount of points to change
    :type student: StudentUser
    :type op: str
    :type amount: int
    :rtype: None
    """
    if op=='add':
        student.total_points += amount
    elif op=='sub':
        student.total_points -= amount
    hero = Hero.objects.filter(price__lte=student.total_points).order_by('-price')[0]
    if student.current_hero != hero:
        student.current_hero = hero
    student.save()

def unlockHint(request):
    """
    Unlocks hint for student and record it. Used for Challenge Mode

    :param request: submitted request
    :type request: HttpRequest
    :return: returns whether the hint is successfully opened
    :rtype: JsonResponse
    """
    student = request.user.studentuser
    data = {
    }
    if request.method == 'POST':
        hint_id = request.POST.get('id')
        hint = Hint.objects.get(id=hint_id)
        if HintOpened.objects.filter(student=student, hint=hint).exists():
            data['failed'] = True
        elif student.total_points < hint.points_cost:
            data['failed'] = True
        else:
            unlock = HintOpened(student=student, hint=hint)
            unlock.save()
            changePoints(student, op="sub", amount=hint.points_cost)
        return JsonResponse(data)
    return JsonResponse(data, status=404)

def submitMcqAjax(request, mode, set_id, question_id):
    """
    Processes MCQ submission. Calculate score if Challenge mode, and creates either Submission or TestSubmission

    :param request: submitted request
    :param mode: the current mode, Practice, Test, or Challenge
    :param set_id: the current set id
    :param question_id: the current question id
    :type request: HttpRequest
    :type mode: str
    :type set_id: int
    :type question_id: int
    :return: returns whether the answer is successfully submitted
    :rtype: JsonResponse
    """
    question = Question.objects.get(pk=question_id)
    set = Set.objects.get(id=set_id)
    user = request.user.studentuser
    if request.method == 'POST':
        answer = request.POST.get('choice')
        choice = Choice.objects.get(id=answer)
        score = 100 if choice.correct else 0
        context = {
            'correct': choice.correct,
            'choice': choice.choice,
            'score': score,
        }
        # refactor later
        if not hasattr(set, 'testset'):
            s = Submission(student=user, set=set, question=question, score=score,
                           submission_time=datetime.datetime.now(), choice=choice)
            s.save()
        if hasattr(set, 'gameset'):
            gameResult = gameSubmissionCalc(student=user, score=score, points=question.points_reward,
                                            set=set, question=question)
            context.update(gameResult)
        elif hasattr(set, 'testset'):
            studentDoTest = StudentDoTest.objects.filter(student=user, set=set.testset, end__gte=datetime.datetime.now())[0]
            s = TestSubmission(student=user, set=set, question=question, choice=choice,
                               score=score,
                               submission_time=datetime.datetime.now(),
                               student_do_test=studentDoTest)
            s.save()
            # Get max score of each question
            score_values = TestSubmission.objects.filter(student=user, set=set).values('question').annotate(max_score=Max('score'))
            max_scores = [d['max_score'] for d in score_values]
            avg_score = sum(max_scores) / set.question_count
            studentDoTest.marks = avg_score
            studentDoTest.save()
    return JsonResponse(status=200, data=context)

def submitCodeAjax(request, mode, set_id, question_id):
    """
    Processes code completion submission. Calculate score if Challenge mode, and creates either Submission or TestSubmission

    :param request: submitted request
    :param mode: the current mode, Practice, Test, or Challenge
    :param set_id: the current set id
    :param question_id: the current question id
    :type request: HttpRequest
    :type mode: str
    :type set_id: int
    :type question_id: int
    :return: returns whether the code is successfully submitted
    :rtype: JsonResponse
    """
    if request.method == 'POST':
        question = Question.objects.get(pk=question_id)
        set = Set.objects.get(id=set_id)
        submission = request.POST.get('submission')
        user = request.user.studentuser
        matric = user.matric
        context = {
            'matric': matric,
            'submission': submission,
            'question': serializers.serialize('json', [question]),
            'mode': mode,
            'sub_type': 'submit'
        }
        if 'check' == request.POST.get('submission_type'):
            test_cases = question.codecompletionquestion.testcase_set.filter(type='Check')
            submissionResult = runDocker(test_cases, submission)
            context.update(submissionResult)
            context['sub_type'] = 'check'
        else:
            test_cases = question.codecompletionquestion.testcase_set.all()
            submissionResult = runDocker(test_cases, submission)
            context.update(submissionResult)
            if not hasattr(set, 'testset'):
                if submissionResult['score']:
                    s = Submission(student=user, set=set, question=question, code=submission,
                                   score=submissionResult['score'],
                                   submission_time=datetime.datetime.now(), test_case_result=' '.join(str(e) for e in submissionResult['success']),
                                   test_case_id=' '.join(str(e) for e in submissionResult['test_cases_id']))
                    s.save()
            if hasattr(set, 'gameset'):
                gameResult = gameSubmissionCalc(student=user, score=submissionResult['score'], points=question.points_reward,
                                                set=set, question=question)
                context.update(gameResult)
            elif hasattr(set, 'testset'):
                studentDoTest = StudentDoTest.objects.filter(student=user, set=set.testset, end__gte=datetime.datetime.now())[0]
                s = TestSubmission(student=user, set=set, question=question, code=submission, score=submissionResult['score'],
                               submission_time=datetime.datetime.now(), test_case_result=' '.join(str(e) for e in submissionResult['success']),
                               test_case_id=' '.join(str(e) for e in submissionResult['test_cases_id']), student_do_test=studentDoTest)
                s.save()
                # Get max score of each question
                score_values = TestSubmission.objects.filter(student=user, set=set).values('question').annotate(max_score=Max('score'))
                max_scores = [d['max_score'] for d in score_values]
                avg_score = sum(max_scores) / set.question_count
                studentDoTest.marks = avg_score
                studentDoTest.save()
        return JsonResponse(status=200, data=context)
        # return HttpResponse(template.render(context, request))

def gameSubmissionCalc(student, score, points, set, question):
    """
    Calculates points earned for Challenge submission. Points are only awarded if score is >75

    :param student: the student
    :param score: the student's score
    :param points: the questions max points awarded
    :param set: the set involved
    :param question: the question involved
    :type student: StudentUser
    :type score: float
    :type points: int
    :type set: Set
    :type question: Question
    :return: returns points awarded
    :rtype: dict
    """
    points = round(score / 100 * points)
    # Passing score > 75
    if points and score > 75 and len(Submission.objects.filter(student=student, set=set, question=question))==1:
        changePoints(student, op="add", amount=points)
    else:
        points = 0
    result = {
        'points' : points,
    }
    return result

def scoreCalc(cmp, flake8_output):
    """
    Calculates score for students.
    Scoring: (# correct test case/# of testcase)*100
    If score is >80, each code styling error outputted from flake8 will deduct 0.4 score

    :param cmp: list of student's code compared with test case
    :param flake8_output: output from running flake8 on student's code
    :type cmp: list
    :type flake8_output: str
    :return: final score
    :rtype: int
    """
    # make sure no division by 0
    cmpLen = len(cmp) if len(cmp) > 0 else 1
    base_score = (cmp.count(True) / cmpLen) * 100
    suggestion_score = round(len(flake8_output.splitlines()) * 0.4)
    suggestion_score = suggestion_score if suggestion_score < 5 else 5
    score = base_score
    # Only deduct suggestion score if score > 80
    if score > 80:
        score = score - suggestion_score
    return score

def runDocker(test_cases, submission):
    """
    Executes batch file that runs Docker container, running the student's code.

    :param test_cases: list of test cases for the question
    :param submission: student's code that is going to be run
    :type test_cases: list
    :type submission: str
    :return: result of execution
    :rtype: dict
    """
    old_dir = os.getcwd()
    os.chdir('./submission')

    # write python file
    with open('codeSubmission.py', 'w') as f:
        # todo remove empty lines
        f.write(str(submission).replace('\r', ''))

    flake8 = subprocess.Popen("suggestion.bat", stdout=PIPE, stderr=PIPE)
    # Sanitize output
    flake8_output = flake8.communicate()[0].decode("utf-8").replace("codeSubmission.py:", "Line ")

    output = []
    cmp = []
    # Execute all test cases
    for test_case in test_cases:
        # append arguments to batch file
        number_of_test_case = test_case.number_of_args
        if test_case.number_of_args:
            with open("submission_template.bat", 'r') as f:
                submission_template = f.read()
            with open("submit.bat", 'w') as f:
                f.write(submission_template)
                for i in range(1, number_of_test_case + 1):
                    f.write(' %{}'.format(i))
            test_input = test_case.input_value
            fh = subprocess.Popen("submit.bat " + test_input, stdout=PIPE, stderr=PIPE)
        else:
            fh = subprocess.Popen("submit.bat", stdout=PIPE, stderr=PIPE)
        current_output = (fh.communicate()[0].decode("utf-8"))
        if not current_output:
            output.append(fh.communicate()[1].decode("utf-8"))
        else:
            output.append(current_output)
        with open('result.txt', 'w') as f:
            f.write(str(output[-1]).rstrip().replace('\r', ''))
        with open('correct_result.txt', 'w') as f:
            f.write(str(test_case.output_value).replace('\r', ''))
        cmp.append(filecmp.cmp('result.txt', 'correct_result.txt'))
    os.chdir(old_dir)

    finalScore = scoreCalc(cmp, flake8_output)
    result = {
        'number_of_test_cases': len(test_cases),
        'test_cases': serializers.serialize('json', test_cases),
        'output': output,
        'success': cmp,
        'score': finalScore,
        'suggestion': flake8_output,
        'test_cases_id': [i for i in test_cases.values_list('id', flat=True)],
        'test_cases_output': [i.replace('\r', '') for i in test_cases.values_list('output_value', flat=True)],
    }
    return result
