from django import template

register = template.Library()

# @register.filter
def get_index(l, i):
    return l[i]

def check_list(item, l2):
    print(type(item))
    print(l2)
    print(item in l2)
    return item in l2

def subtract(value, arg):
    return value - arg

def divide(value, arg):
    try:
        return int((int(value) / int(arg))*100)
    except (ValueError, ZeroDivisionError):
        return None

def percentof(amount, total):
    try:
        return '{:.1f}%'.format(amount / total * 100)
    except ZeroDivisionError:
        return None

def is_false(arg):
    return arg is False

register.filter('subtract', subtract)
register.filter('divide', divide)
register.filter('check_list', check_list)
register.filter('get_index', get_index)
register.filter('percentof', percentof)
register.filter('is_false', is_false)