from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.template.defaultfilters import truncatewords
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from django.db.models import Count, Sum, Avg, Max, Min
from django.db.models.signals import post_save
from django.dispatch import receiver
import os

EASY = 'Easy'
MEDIUM = 'Medium'
HARD = 'Hard'
DIFFICULTY_CHOICES = (
    (EASY, 'Easy'),
    (MEDIUM, 'Medium'),
    (HARD, 'Hard'),
)

class StudentUser(models.Model):
    """
    Extends User model to keep track of student's personal details

    :cvar GENDER_CHOICES: shortens Female and Male to F and M for database storage
    :ivar user: one-to-one relationship with User, StudentUser extends User
    :ivar full_name: full name
    :ivar group: student lab group
    :ivar total_points: accumulated points
    :ivar question: many-to-many relationship with Question
    :ivar char_gender: keeps track of character gender in challenge mode
    """
    GENDER_CHOICES = (
        ('F', 'Female'),
        ('M', 'Male'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    matric = models.CharField(max_length=200, null=True, blank=True)
    full_name = models.CharField(max_length=200, null=True, blank=True)
    group = models.CharField(max_length=200, null=True, blank=True)
    total_points = models.IntegerField(null=True, blank=True, default=0)
    current_hero = models.ForeignKey('Hero', on_delete=models.SET_NULL, default=1, null=True, blank=True)
    test = models.ManyToManyField('TestSet', through='StudentDoTest')
    question = models.ManyToManyField('Question', through='Submission')
    char_gender = models.CharField(
        max_length=2,
        choices=GENDER_CHOICES,
        default='M',
    )

    def __str__(self):
        if self.matric:
            return self.matric
        else:
            return str(self.id)

    def ranking(self):
        """
        Calculates the student's ranking

        :return: StudentUser's ranking
        :rtype: int
        """
        aggregate = StudentUser.objects.filter(total_points__gt=self.total_points).aggregate(ranking=Count('total_points'))
        return aggregate['ranking'] + 1

class GameCanvas(models.Model):
    """
    Stores images of objects used in Challenge Mode

    :ivar name: object name
    :ivar image: image of object
    :ivar pos_x: x position of object
    :ivar pos_y: y position of object
    :ivar rank_req: rank required to unlock object
    """
    name = models.CharField(max_length=1000, null=True, blank=True)
    image = models.ImageField(upload_to='game_canvas',null=True, blank=True)
    pos_x = models.IntegerField(null=True, blank=True, default=0)
    pos_y = models.IntegerField(null=True, blank=True, default=0)
    rank_req = models.ForeignKey('Hero', on_delete=models.SET_NULL, null=True, blank=True)
    def __str__(self):
        return str(self.id)
    class Meta:
         verbose_name = "Game Asset"

# A set of questions
class Set(models.Model):
    """
    A set stores multiple questions

    :ivar name: name of set
    :ivar question: many-to-many relationship with Question
    """
    name = models.CharField(max_length=1000, null=True, blank=True)
    question = models.ManyToManyField('Question')
    def __str__(self):
        return self.name
    @property
    def question_count(self):
        """
        Calculates the count of questions in a set
        :return: number of questions in the set
        :rtype: int
        """
        return len(self.question.all())

class PracticeSet(Set):
    """
    Subclass of set, contains sets in Practice mode
    """
    pass

class TestSet(Set):
    """
    Subclass of set, contains sets in Test mode

    :ivar time_limit: how long the student has to complete the test
    :ivar start_time: the earliest time the set becomes available
    :ivar end_time: the latest time for submissions to be made
    :ivar allow_avg: allow class average to be shown to students
    :ivar allow_set_score: allow score to be shown to students
    :ivar allow_set_details: allow set details to be shown to students
    :ivar allow_qn_details: allow question details to be shown to students (requires allow_set_details to be True)
    :ivar allow_test_case: allow test case details to be shown to students (requires allow_qn_details to be True)
    :ivar allow_qn_score: allow question score to be shown to students (requires allow_qn_details to be True)
    """
    time_limit = models.PositiveIntegerField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    end_time = models.DateTimeField(null=True, blank=True)
    status = models.NullBooleanField(null=True, blank=True)
    allow_avg = models.NullBooleanField(null=True, blank=True)
    allow_set_score = models.NullBooleanField(null=True, blank=True)
    allow_set_details = models.NullBooleanField(null=True, blank=True)
    allow_qn_details = models.NullBooleanField(null=True, blank=True)
    allow_test_case = models.NullBooleanField(null=True, blank=True)
    allow_qn_score = models.NullBooleanField(null=True, blank=True)

class TestSetSummary(TestSet):
    class Meta:
        proxy = True

class GameSet(Set):
    """
    Subclass of set, contains sets in Game mode

    :ivar set_difficulty: the difficulty of a set, equal to minimum proficiency needed to unlock the set
    """
    set_difficulty = models.FloatField(null=True, blank=True)
    class Meta:
         verbose_name = "Challenge Set"

class HintOpened(models.Model):
    """
    Records if hint has been opened by a student

    :ivar student: foreign key to StudentUser
    :ivar hint: foreign key to Hint
    """
    student = models.ForeignKey(StudentUser, on_delete=models.SET_NULL, null=True, blank=True)
    hint = models.ForeignKey('Hint', on_delete=models.SET_NULL, null=True, blank=True)
    def __str__(self):
        return str(self.id)

class StudentDoTest(models.Model):
    """
    Records if a student has done a TestSet

    :ivar student: foreign key to StudentUser
    :ivar end: the latest time the student's submission will be accepted
    :ivar set: foreign key to TestSet
    :ivar marks: the average marks for all questions in the set
    """
    student = models.ForeignKey(StudentUser, on_delete=models.SET_NULL, null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    set = models.ForeignKey(TestSet, on_delete=models.SET_NULL, null=True, blank=True)
    marks = models.IntegerField(null=True, blank=True)
    def __str__(self):
        return str(self.id)
    def avg(self):
        """
        Calculates the average mark from all questions in the test set

        :return: average mark
        :rtype: int
        """
        aggregate = StudentDoTest.objects.filter(set=self.set).aggregate(avg=Avg('marks'))
        return aggregate['avg']
    class Meta:
         verbose_name = "Student test attempt"

class Hero(models.Model):
    """
    Rank of a student, used in Challenge Mode

    :ivar name: rank name
    :ivar proficiency: decides which game items are unlocked, and which ChallengeSets can be accessed
    :ivar price: points required to unlock the rank
    """
    name = models.CharField(max_length=1000, null=True, blank=True)
    proficiency = models.FloatField(null=True, blank=True)
    price = models.IntegerField(null=True, blank=True)
    def __str__(self):
        return self.name
    class Meta:
         verbose_name_plural = "Heroes"

class Question(models.Model):
    """
    A question object stores all details of a question

    :ivar difficulty: difficulty of a question: Easy, Medium, Hard
    :ivar question: question title
    :ivar explanation: detailed explanation of the question
    :ivar points_reward: points rewarded for finishing the question, used in Challenge mode
    :ivar tag: many-to-many relationship with Tag
    """
    difficulty = models.CharField(choices=DIFFICULTY_CHOICES, max_length=20, blank=True, null=True)
    question = models.TextField(blank=True, null=True)
    explanation = MarkdownxField(blank=True, null=True)
    points_reward = models.IntegerField(null=True, blank=True)
    tag = models.ManyToManyField('Tag', related_name='questions')

    @property
    def set_display(self):
        diff = {
            'Easy': 'E',
            'Medium': 'M',
            'Hard': 'H'
        }
        return ("({}) {}".format(diff[self.difficulty], self.question))
    @property
    def short_description(self):
        """
        Truncates the explanation of a question into 15 char
        """
        return truncatewords(self.explanation, 15)

    @property
    def tag_comma(self):
        """
        Joins all question tags with comma
        """
        tag = [tag.tag for tag in self.tag.all()]
        return ", ".join(tag)

    def __str__(self):
        return self.question

class Tag(models.Model):
    """
    Topics assigned to a question

    :ivar tag: Topic name
    """
    tag = models.CharField(max_length=100)
    def __str__(self):
        return self.tag

class MultiChoiceQuestion(Question):
    """
    Subclass of Question, used for multiple-choice questions
    """
    pass
    class Meta:
        verbose_name = "Multiple choice question"

class CodeCompletionQuestion(Question):
    """
    Subclass of Question, used for code-completion questions

    :ivar default_text: text to be displayed initially in the code editor
    """
    default_text = models.TextField(null=True, blank=True)

class Hint(models.Model):
    """
    Hints for a question, used in Practice and Challenge mode

    :ivar question: foreign key to Question
    :ivar text: hint content
    :ivar points_cost: only used in Challenge mode, deducts points on opening hint
    """
    question = models.ForeignKey('Question', on_delete=models.SET_NULL, null=True)
    text = models.TextField(null=True, blank=True)
    points_cost = models.IntegerField(null=True, blank=True)
    def __str__(self):
        return self.text

class Choice(models.Model):
    """
    Available choices in a multiple-choice question

    :ivar question: foreign key to Question
    :ivar choice: choice content
    :ivar correct: choice correctness
    """
    question = models.ForeignKey('MultiChoiceQuestion', on_delete=models.SET_NULL, null=True, blank=True)
    choice = models.TextField("Choice", null=True, blank=True)
    correct = models.NullBooleanField()
    def __str__(self):
        return self.choice

class TestCase(models.Model):
    """
    Available test cases in a code-completion question

    :ivar question: foreign key to Question
    :ivar number_of_args: number of arguments for the test case
    :ivar input_value: test case input
    :ivar output_value: test case output
    :ivar type: is the test case used in check or real submission
    """
    CHECK = 'Check'
    REAL = 'Real'
    TYPE = (
        (CHECK, 'Check'),
        (REAL, 'Real'),
    )
    question = models.ForeignKey('CodeCompletionQuestion', on_delete=models.SET_NULL, null=True, blank=True)
    number_of_args = models.IntegerField(null=True, blank=True)
    input_value = models.TextField(null=True, blank=True)
    output_value = models.TextField(null=True, blank=True)
    type = models.CharField(choices=TYPE, max_length=20, blank=True, null=True)
    def __str__(self):
        return str(self.id)

class Submission(models.Model):
    """
    Stores student's submission

    :ivar student: foreign key to StudentUser
    :ivar question: foreign key to Question
    :ivar set: foreign key to Set
    :ivar code: code submitted by student
    :ivar choice: choice selected by student
    :ivar submission_time: timestamp for submission
    :ivar test_case_id: test cases that are used in this submission
    :ivar test_case_result: result of test cases
    """
    student = models.ForeignKey(StudentUser, on_delete=models.SET_NULL, null=True)
    question = models.ForeignKey('Question', on_delete=models.SET_NULL, null=True)
    set = models.ForeignKey('Set', on_delete=models.SET_NULL, null=True)
    code = models.TextField(null=True, blank=True)
    choice = models.ForeignKey('Choice', on_delete=models.SET_NULL, null=True, blank=True)
    score = models.IntegerField(null=True, blank=True)
    submission_time = models.DateTimeField(null=True, blank=True)
    test_case_id = models.CharField(max_length=10000, null=True, blank=True)
    test_case_result = models.CharField(max_length=10000, null=True, blank=True)

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse('submission-detail', args=[str(self.id)])

class TestSubmission(Submission):
    """
    Subclass of Submission

    :ivar student_do_test: foreign key to StudentDoTest
    :ivar feedback: instructor feedback for student
    """
    student_do_test = models.ForeignKey('StudentDoTest', on_delete=models.SET_NULL, null=True, blank=True)
    feedback = MarkdownxField(blank=True, null=True)

    @property
    def formatted_markdown(self):
        return markdownify(self.feedback)